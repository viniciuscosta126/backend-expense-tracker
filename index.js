//Configuração inicial

const express = require("express");
const mongoose = require("mongoose");
const app = express();

const itemRoutes = require('./routes/itemRoutes')
const categoriaRoutes = require('./routes/categoriaRotas')
//Ler JSON /middlewares

app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json());
app.use('/item',itemRoutes)
app.use('/categoria',categoriaRoutes)
//rota Inicial

mongoose
  .connect(
    "mongodb://viniciuscosta2:1935mimo@projet1-shard-00-00.kt4jv.mongodb.net:27017,projet1-shard-00-01.kt4jv.mongodb.net:27017,projet1-shard-00-02.kt4jv.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-ek5jtx-shard-0&authSource=admin&retryWrites=true&w=majority"
  )
  .then(() => {
    console.log("Conectado ao mongo db");
    app.listen(3000);
  })
  .catch((err) => console.log(err));
