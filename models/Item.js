const moongose = require("mongoose");

const Item = moongose.model("Item", {
    date: Date,
    category:String,
    title:String,
    value:Number
});


module.exports = Item