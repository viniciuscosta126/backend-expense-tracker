const router = require("express").Router();
const Categoria = require("../models/Categoria");

router.post("/cadastrar", async (req, res) => {
  const { title, cor, expence } = req.body;

  const categoria = {
    title,
    cor,
    expence,
  };

  try {
    await Categoria.create(categoria);
    res.status(200).json({ message: "Categoria criada", categoria });
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

router.get('/', async (req,res)=>{

    try {
        const categoria = await Categoria.find()
    res.status(200).json(categoria);
    } catch (error) {
        res.status(500).json({ error: error });
    }
})

router.patch('/update/:id', async (req,res)=>{
    const {id} = req.params
    const { title, cor, expence } = req.body;
    const categoria = {
        title,
        cor,
        expence,
      };

      try {
        const updateCategoria = await Categoria.updateOne({ _id: id }, categoria);
        res.status(200).json(updateCategoria)
      } catch (error) {
        res.status(500).json({ error: error });
      }
})
router.delete('/delete/:id',async (req,res)=>{
    const {id} = req.params
    try {
        const deleteCategory = await Categoria.deleteOne({_id:id})
        res.status(200).json({message:"Deletado com sucesso"})
    } catch (error) {
        res.status(500).json({ error: error });
    }
})
module.exports = router;
