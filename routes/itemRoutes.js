const router = require("express").Router();
const Item = require("../models/Item");

router.post("/store", async (req, res) => {
  const { date, category, title, value } = req.body;

  const item = {
    date,
    category,
    title,
    value,
  };
  try {
    await Item.create(item);
    res.status(200).json({ message: "item criado com sucesso" });
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

router.get("/store", async (req, res) => {
  try {
    /*const id = req.params.id
        const item = await Item.findOne({_id:id})*/
    const item = await Item.find();
    res.status(200).json(item);
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

router.patch("/store/update/:id", async (req, res) => {
  const id = req.params.id;
  const { date, category, title, value } = req.body;
  const item = {
    date,
    category,
    title,
    value,
  };
  try {
    const updatedItem = await Item.updateOne({ _id: id }, item);
    res.status(200).json(item);
  } catch (error) {
    res.status(500).json({ error: error });
  }
});
router.delete("/store/delete/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const deleteItem = await Item.deleteOne({ _id: id });
    res.status(200).json({ message: "item deletado" });
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

module.exports = router;
